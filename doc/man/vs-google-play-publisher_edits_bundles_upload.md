## vs-google-play-publisher edits bundles upload



```
vs-google-play-publisher edits bundles upload [flags]
```

### Options

```
      --file string   file to upload
  -h, --help          help for upload
```

### SEE ALSO

* [vs-google-play-publisher edits bundles](vs-google-play-publisher_edits_bundles.md)	 - 

