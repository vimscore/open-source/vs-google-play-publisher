## vs-google-play-publisher edits tracks update



```
vs-google-play-publisher edits tracks update [flags]
```

### Options

```
  -h, --help                            help for update
      --release-name string             release name
      --release-notes string            release notes
      --release-notes-language string   language of release notes (en_US etc)
      --remove                          Remove release from list of releases on track
      --status string                   the edit's status. Defaults to draft
      --track string                    track to publish on
      --version-code int                version code (default -1)
```

### SEE ALSO

* [vs-google-play-publisher edits tracks](vs-google-play-publisher_edits_tracks.md)	 - 

