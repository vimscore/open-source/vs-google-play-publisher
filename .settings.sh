#!/bin/sh

if [ "$0" = "$BASH_SOURCE" ]
then
  echo "This script should be sourced"
  exit 1
fi

basedir="$(pwd)"

PATH="$basedir/bin:$PATH"
