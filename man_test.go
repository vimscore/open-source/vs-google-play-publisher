package main

import (
	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
	"log"
	"os"
	"testing"
	"vimscore.com/vs-google-play-publisher/cmd"
)

var markdownDirName = "doc/man"

func Test_GenerateMarkdown(t *testing.T) {
	fix(cmd.RootCmd())

	err := os.RemoveAll(markdownDirName)
	if err != nil {
		t.Error(err)
	}

	err = os.MkdirAll(markdownDirName, os.ModePerm)
	if err != nil {
		t.Error(err)
	}

	err = doc.GenMarkdownTree(cmd.RootCmd(), markdownDirName)
	if err != nil {
		log.Fatal(err)
	}
}

func fix(cmd *cobra.Command) {
	cmd.DisableAutoGenTag = true
	for _, child := range cmd.Commands() {
		fix(child)
	}
}
