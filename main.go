package main

import (
	"os"
	"vimscore.com/vs-google-play-publisher/cmd"
	"vimscore.com/vs-google-play-publisher/internal"
)

var version = ""
var date = ""
var commit = ""

func main() {
	internal.Init(version, date, commit)
	if err := cmd.RootCmd().Execute(); err != nil {
		os.Exit(1)
	}
}
