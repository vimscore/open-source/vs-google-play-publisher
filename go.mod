module vimscore.com/vs-google-play-publisher

go 1.15

require (
	github.com/joho/godotenv v1.4.0
	github.com/mattn/go-isatty v0.0.3
	github.com/olekukonko/tablewriter v0.0.5
	github.com/spf13/cobra v1.1.3
	google.golang.org/api v0.42.0
)
