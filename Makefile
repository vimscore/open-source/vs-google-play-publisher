BIN=vs-google-play-publisher
GOSRC=$(shell find . -name \*.go)
export MAKEFLAGS=-r

all: $(BIN)
.PHONY: all

$(BIN): $(GOSRC)
	bin/vs-gpp-build "$@"

dist/$(BIN)-%: $(GOSRC)
	@export ARCH=$(subst dist/$(BIN)-,,$@); export CGO_ENABLED; \
	echo "Building for $${ARCH}"; \
	bin/vs-gpp-build "$@"

test:
	go test

check-changes:
	@[ -z "$(shell git status --porcelain)" ] || (git status; false)

clean:
	rm -f $(wildcard $(BIN)*)
	rm -rf dist

export CGO_ENABLED

dist/$(BIN)-Linux-x86_64: export CGO_ENABLED=0

dist: dist/$(BIN)-Linux-x86_64
dist: dist/$(BIN)-Linux-arm64
dist: dist/$(BIN)-Darwin-x86_64
dist: dist/$(BIN)-Darwin-arm64
dist: dist/$(BIN)-Windows-x86_64
#dist: dist/$(BIN)-Windows-arm64
.PHONY: bin
