package internal

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/mattn/go-isatty"
	"github.com/spf13/cobra"
	"log"
	"os"
	"strconv"
)

const CmdName = "vs-google-play-publisher"
const StateFile = "." + CmdName
const StateVarPackage = "PACKAGE"
const StateVarEditId = "EDIT_ID"
const StateVarTrack = "TRACK"
const StateVarVersionCode = "VERSION_CODE"

var RootCmd = &cobra.Command{Use: CmdName, SilenceUsage: true}
var Args RootArgs

var state = make(map[string]string)

type RootArgs struct {
	package_ string
	editId   string
	format   string
}

func (args *RootArgs) Package() string {
	if args.package_ == "" {
		args.package_, _ = state[StateVarPackage]
	}

	if args.package_ == "" {
		_, _ = fmt.Fprintln(os.Stderr, "'package' is required. Either specify --package or put it in the state file")
		os.Exit(1)
	}
	return args.package_
}

func (args *RootArgs) EditId() string {
	if args.editId == "" {
		args.editId = state[StateVarEditId]
	}

	if args.editId == "" {
		_, _ = fmt.Fprintln(os.Stderr, "'edit-id' is required. Either specify --edit-id or put it in the state file")
		os.Exit(1)
	}

	return args.editId
}

func (args *RootArgs) Format() string {
	if args.format == "auto" {
		tty := isatty.IsTerminal(os.Stdout.Fd())

		if tty {
			args.format = "plain"
		} else {
			args.format = "json"
		}
	} else if !(args.format == "json" || args.format == "plain") {
		args.format = "plain"
	}

	return args.format
}

func (args *RootArgs) Get(cmd *cobra.Command, flagName string, varName string, required bool) (value string) {
	flag := cmd.Flag(flagName)
	if flag.Changed {
		value = flag.Value.String()
	}

	if value == "" {
		value = state[varName]
	}

	if value == "" && required {
		_, _ = fmt.Fprintln(os.Stderr, "'"+flagName+"' is required. Either specify --"+flagName+" or put it in the state file")
		os.Exit(1)
	}

	return value
}

func (args *RootArgs) GetInt64(cmd *cobra.Command, flagName string, varName string, required bool) (value int64) {
	s := args.Get(cmd, flagName, varName, required)

	if s == "" {
		return 0
	}

	value, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "'"+flagName+"' is must be an integer")
		os.Exit(1)
	}

	return
}

var completionCmd = &cobra.Command{
	Use:   "completion",
	Short: "Generates bash completion scripts",
	Long: `To load completion run

. <(` + CmdName + ` completion)

To configure your bash shell to load completions for each session add to your bashrc

# ~/.bashrc or ~/.profile
. <(` + CmdName + ` completion)
`,
	Run: func(cmd *cobra.Command, args []string) {
		err := RootCmd.GenBashCompletion(os.Stdout)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "%v", err)
		}
	},
}

func Init(version, date, commit string) {
	var v string
	//goland:noinspection GoBoolExpressions
	if len(version) > 0 {
		v = version
	} else {
		v = "local"

		if len(date) > 0 {
			v += ", date=" + date
		}

		if len(commit) > 0 {
			v += ", git=" + commit
		}
	}

	RootCmd.Version = v

	RootCmd.PersistentFlags().StringVar(&Args.package_, "package", "",
		"package name, defaults to "+StateVarPackage+" from the state file")
	RootCmd.PersistentFlags().StringVar(&Args.editId, "edit-id", "",
		"edit ID, defaults to "+StateVarEditId+" from the state file")
	RootCmd.PersistentFlags().StringVar(&Args.format, "format", "auto",
		"Output format: plain, json or auto")

	RootCmd.AddCommand(completionCmd)
	LoadState()
}

func LoadState() {
	_, err := os.Stat(StateFile)
	if err == nil {
		state, err = godotenv.Read(StateFile)
		if err != nil {
			log.Fatal(fmt.Errorf("could not load state: %w", err))
		}
	}
}

func SetState(key, value string) {
	if value == "" {
		delete(state, key)
	} else {
		state[key] = value
	}
	err := godotenv.Write(state, StateFile)

	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "Could not write state file")
		os.Exit(1)
	}
}
