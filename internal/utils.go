package internal

import (
	"context"
	"fmt"
	"google.golang.org/api/androidpublisher/v3"
	"io"
)

func NewService() (svc *androidpublisher.Service, err error) {
	svc, err = androidpublisher.NewService(context.Background())
	if err != nil {
		err = fmt.Errorf("could not create publisher service: %w", err)
	}

	return
}

func CloseSilently(closer io.Closer) {
	_ = closer.Close()
}
