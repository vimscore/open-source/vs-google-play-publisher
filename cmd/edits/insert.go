package edits

import (
	"fmt"
	"github.com/spf13/cobra"
	"vimscore.com/vs-google-play-publisher/internal"
)

func runInsert(_ *cobra.Command, _ []string) error {
	svc, err := internal.NewService()
	if err != nil {
		return err
	}

	response, err := svc.Edits.Insert(internal.Args.Package(), nil).Do()
	if err != nil {
		return fmt.Errorf("could not create create edit: %w\n", err)
	}

	internal.SetState(internal.StateVarEditId, response.Id)
	internal.SetState(internal.StateVarPackage, internal.Args.Package())

	return nil
}

func init() {
	c := &cobra.Command{Use: "insert", RunE: runInsert}
	Cmd.AddCommand(c)
}
