package edits

import (
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"google.golang.org/api/androidpublisher/v3"
	"vimscore.com/vs-google-play-publisher/internal"
)

func runCommit(_ *cobra.Command, _ []string) error {
	svc, err := androidpublisher.NewService(context.Background())
	if err != nil {
		return fmt.Errorf("could not create publisher service: %w", err)
	}

	_, err = svc.Edits.Commit(internal.Args.Package(), internal.Args.EditId()).Do()
	if err != nil {
		return fmt.Errorf("could not commit edit: %w", err)
	}

	fmt.Println("Edit committed.")

	internal.SetState(internal.StateVarEditId, "")

	return nil
}

func init() {
	c := &cobra.Command{Use: "commit", RunE: runCommit}
	Cmd.AddCommand(c)
}
