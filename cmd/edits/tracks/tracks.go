package tracks

import (
	"github.com/spf13/cobra"
	"vimscore.com/vs-google-play-publisher/cmd/edits"
)

var tracksCmd = &cobra.Command{
	Use: "tracks",
}

func init() {
	edits.Cmd.AddCommand(tracksCmd)
}
