package tracks

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"google.golang.org/api/androidpublisher/v3"
	"google.golang.org/api/googleapi"
	"log"
	"vimscore.com/vs-google-play-publisher/internal"
)

var update = struct {
	versionCode          int64
	track                string
	releaseName          string
	releaseNotes         string
	releaseNotesLanguage string
	status               string
	remove               bool
}{}

func updateRun(cmd *cobra.Command, _ []string) (err error) {
	svc, err := internal.NewService()
	if err != nil {
		return
	}

	trackS := internal.Args.Get(cmd, "track", internal.StateVarTrack, true)
	versionCode := internal.Args.GetInt64(cmd, "version-code", internal.StateVarVersionCode, false)

	if update.remove {
		return updateRemove(trackS, svc)
	}

	track, err := svc.Edits.Tracks.Get(internal.Args.Package(), internal.Args.EditId(), trackS).Do()
	if err != nil {
		if apiError, ok := err.(*googleapi.Error); !ok || apiError.Code != 404 {
			return
		}
	}
	if track == nil {
		log.Println("No track found, creating stub")
		track = &androidpublisher.Track{
			Track: trackS,
		}
	} else {
		dumpTrack(track)
	}

	var release *androidpublisher.TrackRelease
	for _, r := range track.Releases {
		if r.Name == update.releaseName {
			log.Println("Found existing release")
			release = r
		}
	}

	if release == nil {
		release = &androidpublisher.TrackRelease{}
		track.Releases = append(track.Releases, release)
		log.Println("Creating release")
	}

	release.Name = update.releaseName

	if update.status != "" {
		release.Status = update.status
	}

	if versionCode > 0 {
		release.VersionCodes = []int64{versionCode}
	}

	if update.releaseNotes != "" {
		var releaseNotes *androidpublisher.LocalizedText
		for _, rn := range release.ReleaseNotes {
			if rn.Language == update.releaseNotesLanguage {
				log.Println("Updating release notes")
				releaseNotes = rn
			}
		}

		if releaseNotes == nil {
			log.Println("Creating release notes")
			releaseNotes = &androidpublisher.LocalizedText{}
			release.ReleaseNotes = append(release.ReleaseNotes, releaseNotes)
		}

		releaseNotes.Language = update.releaseNotesLanguage
		releaseNotes.Text = update.releaseNotes
	}

	dumpTrack(track)

	track, err = svc.Edits.Tracks.Update(internal.Args.Package(), internal.Args.EditId(), trackS, track).Do()

	//dumpTrack(track)

	return
}

func updateRemove(trackS string, svc *androidpublisher.Service) (err error) {
	track, err := svc.Edits.Tracks.Get(internal.Args.Package(), internal.Args.EditId(), trackS).Do()
	if err != nil {
		if apiError, ok := err.(*googleapi.Error); ok || apiError.Code == 404 {
			log.Println("No such track, no releases to remove")
			return
		}
	}

	if update.releaseName == "" && update.status == "" {
		log.Println("Removing all releases from track")
		track.Releases = []*androidpublisher.TrackRelease{}
	} else {
		found := false
		for i, release := range track.Releases {
			if (update.releaseName == "" || release.Name == update.releaseName) &&
				(update.status == "" || release.Status == update.status) {

				copy(track.Releases[i:], track.Releases[i+1:])
				track.Releases = track.Releases[:len(track.Releases)-1]
				found = true

				log.Println("Removing release from track")
			}
		}

		if !found {
			return fmt.Errorf("release not found")
		}
	}

	dumpTrack(track)

	track, err = svc.Edits.Tracks.Update(internal.Args.Package(), internal.Args.EditId(), trackS, track).Do()

	dumpTrack(track)

	return
}

func dumpTrack(track *androidpublisher.Track) {
	bytes, _ := json.MarshalIndent(track, "", "  ")
	log.Println("---------- Track ----------")
	log.Printf("\n%s", string(bytes))
}

func init() {
	var c = &cobra.Command{Use: "update", RunE: updateRun}
	tracksCmd.AddCommand(c)

	c.Flags().Int64Var(&update.versionCode, "version-code", -1, "version code")

	c.Flags().StringVar(&update.track, "track", "", "track to publish on")
	_ = c.MarkFlagRequired("track")

	c.Flags().StringVar(&update.releaseName, "release-name", "", "release name")

	c.Flags().StringVar(&update.status, "status", "", "the edit's status. Defaults to draft")

	c.Flags().StringVar(&update.releaseNotes, "release-notes", "", "release notes")
	_ = c.MarkFlagFilename("release-notes")

	c.Flags().StringVar(&update.releaseNotesLanguage, "release-notes-language", "", "language of release notes (en_US etc)")

	c.Flags().BoolVar(&update.remove, "remove", false, "Remove release from list of releases on track")
}
