package tracks

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"strings"
	"vimscore.com/vs-google-play-publisher/internal"
)

func listRun(*cobra.Command, []string) error {
	svc, err := internal.NewService()
	if err != nil {
		return err
	}

	tracksList, err := svc.Edits.Tracks.List(internal.Args.Package(), internal.Args.EditId()).Do()
	if err != nil {
		return fmt.Errorf("could not list tracks: %w", err)
	}

	if internal.Args.Format() == "json" {
		bytes, err := tracksList.MarshalJSON()
		if err != nil {
			return err
		}

		_, err = os.Stdout.Write(bytes)
		return err
	}

	for i, track := range tracksList.Tracks {
		if i > 0 {
			fmt.Println()
		}

		fmt.Printf("Track: %s\n", track.Track)
		fmt.Println()
		for j, release := range track.Releases {
			if j > 0 {
				fmt.Println()
			}

			fmt.Printf("Name:          %s\n", release.Name)
			fmt.Printf("Status:        %s\n", release.Status)
			fmt.Printf("Version codes: ")
			for k, vc := range release.VersionCodes {
				if k > 0 {
					fmt.Printf(", ")
				}
				fmt.Printf("%d", vc)
			}
			fmt.Println()
			fmt.Println()
			for _, rn := range release.ReleaseNotes {
				fmt.Printf("Release notes for %s:\n", rn.Language)

				for _, s := range strings.Split(rn.Text, "\n") {
					fmt.Printf(" %s\n", s)
				}
			}
		}
	}

	return nil
}

func init() {
	c := &cobra.Command{Use: "list", RunE: listRun}
	tracksCmd.AddCommand(c)
}
