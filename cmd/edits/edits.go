package edits

import (
	"github.com/spf13/cobra"
	"vimscore.com/vs-google-play-publisher/internal"
)

var Cmd = &cobra.Command{
	Use: "edits",
}

func init() {
	internal.RootCmd.AddCommand(Cmd)
}
