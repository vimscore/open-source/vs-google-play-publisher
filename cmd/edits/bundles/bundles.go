package bundles

import (
	"github.com/spf13/cobra"
	"vimscore.com/vs-google-play-publisher/cmd/edits"
)

var bundlesCmd = &cobra.Command{
	Use: "bundles",
}

func init() {
	edits.Cmd.AddCommand(bundlesCmd)
}
