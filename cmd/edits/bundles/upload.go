package bundles

import (
	"github.com/spf13/cobra"
	"google.golang.org/api/googleapi"
	"log"
	"math"
	"os"
	"strconv"
	"vimscore.com/vs-google-play-publisher/internal"
)

var upload = struct {
	file string

	size int64
}{}

func uploadRun(_ *cobra.Command, _ []string) (err error) {
	svc, err := internal.NewService()
	if err != nil {
		return
	}

	f, err := os.Open(upload.file)
	if err != nil {
		log.Fatal(err)
	}
	defer internal.CloseSilently(f)

	info, err := f.Stat()
	if err != nil {
		log.Fatal(err)
	}

	upload.size = info.Size()

	log.Println("Uploading artifacts")
	bundle, err := svc.Edits.Bundles.Upload(internal.Args.Package(), internal.Args.EditId()).
		Media(f, googleapi.ChunkSize(128*1024), googleapi.ContentType("application/octet-stream")).
		ProgressUpdater(progressUpdater).
		Do()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("SHA-256", bundle.Sha256)

	internal.SetState("VERSION_CODE", strconv.FormatInt(bundle.VersionCode, 10))

	return nil
}

func progressUpdater(current, total int64) {
	if total == 0 {
		total = upload.size
	}

	if total > 0 {
		log.Printf("Uploaded %d bytes, %d%%\n", current, int32(math.Round((float64(current)/float64(total))*100)))
	} else {
		log.Printf("Uploaded %d bytes\n", current)
	}
}

func init() {
	c := &cobra.Command{Use: "upload", RunE: uploadRun}
	bundlesCmd.AddCommand(c)

	c.Flags().StringVar(&upload.file, "file", "", "file to upload")
	_ = c.MarkFlagFilename("file", "aab", "apk")
}
