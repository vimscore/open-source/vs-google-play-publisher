package bundles

import (
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"log"
	"os"
	"strconv"
	"vimscore.com/vs-google-play-publisher/internal"
)

func listRun(*cobra.Command, []string) error {
	svc, err := internal.NewService()
	if err != nil {
		return err
	}

	log.Println("Listing bundles:")
	bundlesList, err := svc.Edits.Bundles.List(internal.Args.Package(), internal.Args.EditId()).Do()
	if err != nil {
		return err
	}

	if internal.Args.Format() == "json" {
		bytes, err := bundlesList.MarshalJSON()
		if err != nil {
			return err
		}

		_, err = os.Stdout.Write(bytes)
		return err
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Version code", "SHA-1", "SHA-256"})
	for _, bundle := range bundlesList.Bundles {
		table.Append([]string{
			strconv.FormatInt(bundle.VersionCode, 10),
			bundle.Sha1,
			bundle.Sha256,
		})
	}

	table.Render()

	return nil
}

func init() {
	c := &cobra.Command{Use: "list", RunE: listRun}
	bundlesCmd.AddCommand(c)
}
