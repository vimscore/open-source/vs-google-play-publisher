package cmd

import (
	"github.com/spf13/cobra"
	"vimscore.com/vs-google-play-publisher/internal"

	// Make sure all commands are initialized
	_ "vimscore.com/vs-google-play-publisher/cmd/edits"
	_ "vimscore.com/vs-google-play-publisher/cmd/edits/bundles"
	_ "vimscore.com/vs-google-play-publisher/cmd/edits/tracks"
)

func RootCmd() *cobra.Command {
	return internal.RootCmd
}
